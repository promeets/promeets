# Promeets

## General

#### Submodules
* [Chat Service](https://gitlab.com/promeets/chat-service)
* [Cloud Service](https://gitlab.com/promeets/cloud-service)
* [Meeting Service](https://gitlab.com/promeets/meeting-service)
* [Resource Service](https://gitlab.com/promeets/resource-service)
* [Webapp Service](https://gitlab.com/promeets/webapp-service)
* [Promeets Design](https://gitlab.com/promeets/promeets-design)

---

## Development

### Get started
* `git clone https://gitlab.com/promeets/promeets.git && cd promeets`
* `git submodule init && git submodule update`
* (Optional) `git submodule foreach git checkout develop`
* (IDEA) Register all submodules: [IDEA Submodules Registration](https://intellij-support.jetbrains.com/hc/en-us/community/posts/115000083424-How-to-use-IntelliJ-Git-Submodule
                                  )
